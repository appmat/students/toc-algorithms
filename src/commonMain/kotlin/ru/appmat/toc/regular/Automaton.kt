package ru.appmat.toc.regular

import kotlin.math.ln
import kotlin.random.Random

typealias NondeterministicFiniteAutomaton = FiniteAutomaton.NFA
typealias DeterministicFiniteAutomaton = FiniteAutomaton.DFA

sealed class FiniteAutomaton<AutomatonType : FiniteAutomaton<AutomatonType>>(
    open val stateCount: Int
) : Regular<AutomatonType> {

    /**
     * Задача
     * Описать структуру данных ДКА и реализовать dfa-basic
     * Исполнитель: [А-13-17] Ракипов Динаф
     */
    data class DFA(
        override val stateCount: Int,                    //Кол-во состояний
        val alphabet: CharAlphabet,                      //Алфавит
        val transitionsTable: List<Map<Char, Int>>,      //Таблица переходов (таблица смежности)
        val startIndex: Int,                             //Стартовая вершина
        val terminalStates: Set<Int>                     //Терминальные вершины
    ) : FiniteAutomaton<DFA>(stateCount) {

        init {
            if (!(transitionsTable.all { alphabet == it.keys })) {
                throw IllegalArgumentException("Transitions table is not complete or has extra letters")
            }

            val allStates = (0 until stateCount).toSet();

            if (!(transitionsTable.all { it -> it.all { allStates.contains(it.value) } })) {
                throw IllegalArgumentException("Transitions table has transitions to a nonexistent state")
            }
        }

        //region dfa-basic
        override fun check(word: Word): Boolean {
            return check(word, startIndex)
        }

        private tailrec fun check(word: Word, currState: Int): Boolean {
            return when {
                word.isEmpty() -> terminalStates.contains(currState)
                alphabet.contains(word[0]) -> check(
                    word.substring(1, word.length),
                    transitionsTable[currState][word[0]]!!
                )
                else -> false;
            }
        }

        /**
         * Должна расширять алфавит текущего ДКА
         */
        fun withAlphabet(alphabet: CharAlphabet): DFA {

            if (!alphabet.containsAll(this.alphabet)) {
                throw IllegalArgumentException("Method param alphabet does not contain DFA alphabet")
            }

            if (alphabet == this.alphabet) {
                return DFA(stateCount, alphabet, transitionsTable, startIndex, terminalStates)
            }

            val diffAlphabet = alphabet.minus(this.alphabet)
            val newTransitionsTable = transitionsTable.map { it.toMutableMap() }.toMutableList()

            //Создаем тупиковую вершину
            val deadlockIndex: Int = newTransitionsTable.size
            val deadlockState = alphabet.map { it -> Pair<Letter, Int>(it, deadlockIndex) }.toMap().toMutableMap()
            newTransitionsTable.add(deadlockState)

            //Обновляем таблицу переходов
            newTransitionsTable.forEach { state ->
                diffAlphabet.forEach {
                    state[it] = deadlockIndex
                }
            }

            return DFA(stateCount + 1, alphabet, newTransitionsTable, startIndex, terminalStates);
        }

        /**
         * Complement - нетерминалы превращаем в терминалы и наоброт
         * */
        override fun complement(): DFA {
            val newTerminalStates = (0 until stateCount).toSet().minus(terminalStates)
            return DFA(stateCount, alphabet, transitionsTable, startIndex, newTerminalStates);
        }
        //endregion dfa-basic

        fun deleteUnreachableStates(): DFA {
            val reachableStates = getReachablePeaks()// получили вершины, в которые можем попасть
            val unreachableStates = (0 until stateCount).minus(reachableStates)//недоступные, "бесполезные" вершины
            val displacementMap = reachableStates.map { it -> it to 0 }.toMap().toMutableMap()
            //получаем map'у из пар старый номер вершины - новый номер вершины
            var i = 0
            for (j in 0 until stateCount) {
                displacementMap[j] = j - i
                if (unreachableStates.contains(j))
                    i++
            }

            val newTransitionsTable = transitionsTable.map { it.toMutableMap() }.toMutableList()
            unreachableStates.reversed().forEach { newTransitionsTable.removeAt(it) }

            //делаем смещения
            i = 0
            while (i != newTransitionsTable.size) {
                for ((key, value) in newTransitionsTable[i])
                    newTransitionsTable[i][key] = displacementMap[value]!!
                i++
            }

            //смещаем терминальные и стартовое состояния
            val newTerminalStates = terminalStates.minus(unreachableStates).map { displacementMap[it]!! }
            val newStartIndex = displacementMap[startIndex]!!

            return this.copy(
                stateCount = newTransitionsTable.size,
                transitionsTable = newTransitionsTable,
                startIndex = newStartIndex,
                terminalStates = newTerminalStates.toSet()
            )
        }


        private fun getReachablePeaks(): Set<Int> {
            val reached = mutableSetOf<Int>()
            val queue = mutableSetOf<Int>(startIndex)
            queue.add(startIndex)
            while (queue.isNotEmpty()) {
                queue.addAll(transitionsTable[queue.first()].values.minus(reached))
                reached.add(queue.first())
                queue.remove(queue.first())
            }
            return reached
        }

        /**
         * Задача:
         * звезда Клини
         *	Исполнитель: [А-05-17] Павлов Иван
         */
        //region dfa-closure
        override fun starClosure(): DFA {
            TODO("Not yet implemented")
        }

        override fun plusClosure(): DFA {
            TODO("Not yet implemented")
        }
        //endregion dfa-closure


        /**
         * Задача:
         * конкатенация и преобразование в НКА
         * Исполнитель: [А-05-17] Кружкова Мария
         */
        //region dfa-concatenation
        override fun concatenate(other: DFA): DFA {
            val thisNFA = this.toNFA()
            val otherNFA = other.toNFA()

            val newTransitionsTable = thisNFA.transitionsTable.map {
                it.mapValues { (_, state) -> state.toMutableSet() }.toMutableMap()
            }.toMutableList()

            newTransitionsTable.forEachIndexed { index, transition ->
                if (thisNFA.terminalStates.contains(index)) {
                    if ((otherNFA.stateCount == 1)) {
                        newTransitionsTable[index].put(LAMBDA, mutableSetOf(thisNFA.stateCount))
                    } else {
                        transition.forEach { (_, state) ->
                            state.add(thisNFA.stateCount + 1)
                        }
                    }
                }
            }

            newTransitionsTable.addAll(otherNFA.transitionsTable.map { transition ->
                transition.mapValues { (_, state) ->
                    state.map { it + thisNFA.stateCount }.toMutableSet()
                }.toMutableMap()
            })

            return NFA(
                stateCount = thisNFA.stateCount + otherNFA.stateCount,
                transitionsTable = newTransitionsTable.map {
                    it.mapValues { (_, state) -> state.toSet() }.toMap()
                },
                startIndex = thisNFA.startIndex,
                terminalStates = otherNFA.terminalStates.map { states ->
                    states + thisNFA.stateCount
                }.toSet()
            ).toDFA()
        }

        fun toNFA(): NFA {
            return NFA(
                stateCount = stateCount,
                transitionsTable = transitionsTable.map {
                    it.mapValues { (_, state) -> setOf(state) }
                },
                startIndex = startIndex,
                terminalStates = terminalStates
            )
        }
        //endregion


        /**
         * Задача:
         * пересечение и объединение
         * Исполнитель: [A-13-17] Худяков Константин
         */
        //region dfa-intersection

        /**
         * Пересечение двух автоматов представляет из себя новый автомат, в котором:
         * 1) множество состояний - декартово произведение множеств состояний каждого автомата
         * 2) алфавит - объединением двух алфавитов
         * 3) начальная вершина S - вершина, соответствующая декартову произведению начальных вершин
         * 4) множество терминальных состояний - декартово произведение множеств терминальных состояний
         * 5) результирующая вершина при переходе по букве - декартово произведение результирующих вершин
         *      каждого автомата при переходе по этой букве
         */
        override fun intersect(other: DFA): DFA {
            return constructProduct(other, ::createIntersectionTerminalStates)
        }

        /**
         * Объединение двух автоматов представляет из себя новый автомат, в котором:
         * 1) множество состояний - декартово произведение множеств состояний каждого автомата
         * 2) алфавит - объединением двух алфавитов
         * 3) начальная вершина S - вершина, соответствующая декартову произведению начальных вершин
         * 4) множество терминальных состояний - объединение декартовых произведений множества
         *      терминальных состояний первого автомата на множество всех состояний второго
         *      и множества терминальных состояний второго автомата на множество всех состояний первого автомата
         * 5) результирующая вершина при переходе по букве - декартово произведение результирующих вершин
         *      каждого автомата при переходе по этой букве
         */
        override fun union(other: DFA): DFA {
            return constructProduct(other, ::createUnionTerminalStates)
        }

        private fun constructProduct(
            other: DFA,
            terminalStatesProducer: (Set<Int>, Set<Int>, Int, Int) -> Set<Int>
        ): DFA {
            val (first, second) = orderCommutative(this, other)
            val newStateCount = first.stateCount * second.stateCount
            val newAlphabet = first.alphabet.plus(second.alphabet)
            val newTransitionsTable = createDescartesTransitionsTable(first.transitionsTable, second.transitionsTable)
            val newStartIndex = getDescartesStateNum(first.startIndex, second.startIndex, second.stateCount)
            val newTerminalStates = terminalStatesProducer.invoke(
                first.terminalStates,
                second.terminalStates,
                first.stateCount,
                second.stateCount
            )

            return DFA(newStateCount, newAlphabet, newTransitionsTable, newStartIndex, newTerminalStates)
        }

        /**
         * Возвращает пару автоматов, в которой первым будет автомат с меньшем числом состояний
         * (необходима для того, чтобы операции объединения и пересечения были коммутативны по структуре автомата)
         */
        private fun orderCommutative(one: DFA, other: DFA): Pair<DFA, DFA> {
            return if (one.stateCount <= other.stateCount)
                one to other
            else other to one
        }

        /**
         * Преобразует номера состояний двух автоматов в один номер состояния в пересечнии/объединении этих автоматов
         */
        private fun getDescartesStateNum(first: Int, second: Int, secondSize: Int): Int = first * secondSize + second

        private fun createDescartesTransitionsTable(
            one: List<Map<Char, Int>>,
            other: List<Map<Char, Int>>
        ): List<Map<Char, Int>> {
            val stateCount = one.size * other.size
            return (0 until stateCount).map { curState ->
                val oneCurState = curState / other.size
                val otherCurState = curState % other.size
                one[oneCurState].keys
                    .intersect(other[otherCurState].keys)
                    .associateWith {
                        val first = one[oneCurState][it]!!
                        val second = other[otherCurState][it]!!
                        getDescartesStateNum(first, second, other.size)
                    }
            }.toList()
        }

        /**
         * Создаёт множество терминальных состояний, получаемое декартовым произведением множеств двух автоматов
         */
        private fun createIntersectionTerminalStates(
            one: Set<Int>,
            other: Set<Int>,
            oneStateCount: Int,
            otherStateCount: Int
        ): Set<Int> {
            return one.flatMap { terminalState ->
                other.map { terminalState to it }
            }.map { getDescartesStateNum(it.first, it.second, otherStateCount) }.toSet()
        }

        /**
         * Создаёт множество терминальных состояний, получаемое объединением декартовых произведений
         *      множества терминальных состояний первого автомата на множество всех состояний второго
         *      и множества терминальных состояний второго автомата на множество всех состояний первого автомата
         */
        private fun createUnionTerminalStates(
            one: Set<Int>,
            other: Set<Int>,
            oneStateCount: Int,
            otherStateCount: Int
        ): Set<Int> {
            val oldTerminalStates = one.flatMap { terminalState ->
                (0 until otherStateCount).map { terminalState to it }
            } union other.flatMap { terminalState ->
                (0 until oneStateCount).map { it to terminalState }
            }
            return oldTerminalStates.map { getDescartesStateNum(it.first, it.second, otherStateCount) }.toSet()
        }
        //endregion

        /**
         * Задача:
         * ДКА, принимающий реверсию всех слов языка текущего ДКА
         * Исполнитель: [А-13-17] Гвасалия Георгий
         */
        override fun reversed(): DFA = reversedNFA().toDFA()

        fun reversedNFA(): NFA {
            // Reverse all edges in the transition diagram.
            val reversedTransitions = mutableListOf<MutableMap<Char, MutableSet<Int>>>()
            for (i in 0 until stateCount) {
                val reversedMap = mutableMapOf<Char, MutableSet<Int>>()
                for (letter in alphabet)
                    reversedMap[letter] = mutableSetOf()
                reversedTransitions.add(reversedMap)
            }

            transitionsTable.forEachIndexed { i, transition ->
                transition.forEach { (letter, j) -> reversedTransitions[j][letter]!!.add(i) }
            }

            // The accepting state for the L(Reversed) automaton is the start state for the main automaton.
            val reversedTerminalStates = setOf(startIndex)

            // Create a new start state for the new automaton with epsilon transitions to reach of the
            // accept states for the main automaton.
            val lambdaTransitions = mutableMapOf(LAMBDA to terminalStates.toMutableSet())
            reversedTransitions.add(lambdaTransitions)

            val reversedStartIndex = reversedTransitions.lastIndex
            // Convert this NFA back into a DFA.
            return NFA(
                stateCount + 1,
                reversedTransitions,
                reversedStartIndex,
                reversedTerminalStates
            )
        }

        enum class DFAMinimizationAlgorithm {
            Hopcroft, Moore, Brzozowski
        }

        fun minimized(algorithm: DFAMinimizationAlgorithm = DFAMinimizationAlgorithm.Hopcroft): DFA {
            return when (algorithm) {
                DFAMinimizationAlgorithm.Hopcroft -> hopcroftMinimized()
                DFAMinimizationAlgorithm.Moore -> mooreMinimized()
                DFAMinimizationAlgorithm.Brzozowski -> brzozowskiMinimized()
            }
        }

        /**
         * Задача:
         * алгоритм Брзожовски
         * Исполнитель: [A-13-17] Хабаров Павел
         */
        private fun brzozowskiMinimized(): DFA {
            // Преобразуем DFA в NFA, затем реверсируем и детерминизируем
            val reversedDFA = this.toNFA().reversed().toDFA()
            // Полученный DFA снова преобразуем в NFA, еще раз реверсируем и детерминизируем
            return reversedDFA.toNFA().reversed().toDFA()
        }

        /**
         * Задача:
         * алгоритм Мура
         * Исполнитель: [А-13-17] Набиева Ольга
         */
        private fun reversedConnections(currentState: Int, letter: Char): Set<Int> {
            return transitionsTable.indices.filter { transitionsTable[it][letter] == currentState }.toSet()
        }

        private fun getReversedConnectionsTable(): List<Map<Char, Set<Int>>> {
            return transitionsTable.indices.map { state ->
                alphabet.associateWith { letter ->
                    reversedConnections(state, letter)
                }
            }
        }

        private fun getTableOfNonequivalentStates(
            reversedConnections: List<Map<Char, Set<Int>>>
        ): List<List<Boolean>> {

            val queueOfNonequivalentStates: MutableList<Pair<Int, Int>> = mutableListOf()
            val markedPairsOfStates: List<MutableList<Boolean>> = List(stateCount) { MutableList(stateCount) { false } }
            for (i in 0 until stateCount)
                for (j in 0 until stateCount) {
                    if (!markedPairsOfStates[i][j] and (terminalStates.contains(i) != terminalStates.contains(j))) {
                        markedPairsOfStates[i][j] = true
                        markedPairsOfStates[j][i] = true
                        queueOfNonequivalentStates.add(Pair(i, j))
                    }
                }
            while (queueOfNonequivalentStates.isNotEmpty()) {
                val (state1, state2) = queueOfNonequivalentStates[0]
                queueOfNonequivalentStates.removeAt(0)
                for (letter in alphabet) {
                    val reversedConnectionsOfState1 = reversedConnections[state1].getValue(letter)
                    val reversedConnectionsOfState2 = reversedConnections[state2].getValue(letter)
                    val combinationsOfStates = reversedConnectionsOfState1.cartesianProduct(reversedConnectionsOfState2)
                    for ((r, s) in combinationsOfStates)
                        if (!markedPairsOfStates[r][s]) {
                            markedPairsOfStates[r][s] = true
                            markedPairsOfStates[s][r] = true
                            queueOfNonequivalentStates.add(Pair(r, s))
                        }
                }
            }
            return markedPairsOfStates
        }

        private fun <T, R> Iterable<T>.cartesianMap(
            other: Iterable<T>,
            combiner: (T, T) -> R
        ): List<R> {
            return this.flatMap { firstItem -> other.map { secondItem -> combiner(firstItem, secondItem) } }
        }

        private fun <T> Iterable<T>.cartesianProduct(other: Iterable<T>): List<Pair<T, T>> {
            return this.cartesianMap(other) { x, y -> Pair(x, y) }
        }

        private fun getComponentsOfEquivalence(
            reachablePeaks: Set<Int>,
            tableOfNonequivalentStates: List<List<Boolean>>
        ): List<Int> {
            val componentOfEquivalence: MutableList<Int> = MutableList(stateCount) { -1 }
            for (i in 0 until stateCount)
                if (!tableOfNonequivalentStates[0][i])
                    componentOfEquivalence[i] = 0
            var componentsCount = 0
            for (i in 1 until stateCount) {
                if (i !in reachablePeaks)
                    continue
                if (componentOfEquivalence[i] == -1) {
                    componentsCount++
                    componentOfEquivalence[i] = componentsCount
                    for (j in i + 1 until stateCount)
                        if (!tableOfNonequivalentStates[i][j])
                            componentOfEquivalence[j] = componentsCount
                }
            }
            return componentOfEquivalence

        }

        private fun buildDfaFromConponentOfEquivalence(componentOfEquivalence: List<Int>): DFA {
            val newStateCount = componentOfEquivalence.toSet().size
            val newTransitionsTable: List<MutableMap<Char, Int>> =
                List(newStateCount) { mutableMapOf<Char, Int>() }
            for (i in 0 until stateCount) {
                val newState = componentOfEquivalence[i]
                val oldRule = transitionsTable[i]
                for (letter in alphabet) {
                    newTransitionsTable[newState][letter] =
                        newTransitionsTable[newState][letter] ?: componentOfEquivalence[oldRule.getValue(letter)]
                }
            }
            val newStartIndex = componentOfEquivalence[startIndex]
            val newTerminalStates = terminalStates.map { componentOfEquivalence[it] }.toSet()
            return DFA(newStateCount, alphabet, newTransitionsTable, newStartIndex, newTerminalStates)
        }

        private fun mooreMinimized(): DFA {
            val reversedConnections = getReversedConnectionsTable()
            val reachable = getReachablePeaks()
            val tableOfNonequivalentStates = getTableOfNonequivalentStates(reversedConnections)
            val componentOfEquivalence = getComponentsOfEquivalence(reachable, tableOfNonequivalentStates)
            return buildDfaFromConponentOfEquivalence(componentOfEquivalence)
        }

        /**
         * Задача:
         * алгоритм Хопкрофта
         *Исполнитель: [А-05-17] Севостьянов Сергей
         */
        private fun hopcroftMinimized(): DFA {
            TODO("Not yet implemented")
        }

        fun toRightGrammar(): RegularGrammar.RightRegularGrammar = this.toNFA().toRightGrammar()

        fun toLeftGrammar(): RegularGrammar.LeftRegularGrammar = this.toNFA().toLeftGrammar()
    }


    /**
     * Задача:
     * реализовать NFA и nfa-basic
     * Исполнитель: [А-05-17] Морозов Степан
     */
    data class NFA(
        override val stateCount: Int,                    //Кол-во состояний
        val transitionsTable: List<Map<Char, Set<Int>>>,      //Таблица переходов (таблица смежности)
        val startIndex: Int,                             //Стартовая вершина
        val terminalStates: Set<Int>
    ) :
        FiniteAutomaton<NFA>(stateCount) {

        init {
            val allStates = (0 until stateCount).toSet()
            require (transitionsTable.flatMap { it.values }.flatten().all { allStates.contains(it) })
            { "Transitions table has transitions to a nonexistent state" }
        }


        companion object {
            /**
             * Задача:
             * Генератор случайного НКА в подалфавите [a-z]
             * @param alphabetSize          – размер целевого алфавита
             * @param seed                  - сид
             * @param hasLambda             – допустимо ли использовать лямбда-переходы
             * Исполнитель: [А-13-17] Керимов Дмитрий
             */
            private const val STATE_COUNT_LOWER_BOUND = 3
            private const val STATE_COUNT_UPPER_BOUND = 10000
            private const val TRANSITION_COUNT_LOWER_BOUND = 3
            private const val TRANSITION_COUNT_UPPER_BOUND = 10000
            private const val ALPHABET_MAX_SIZE = 26
            fun generate(
                alphabetSize: Int,
                seed: Int = Random.nextInt(),
                hasLambda: Boolean = true
            ): NFA {

                require(alphabetSize in 1..ALPHABET_MAX_SIZE) { "Alphabet size required to be in range 1..26" }

                val random = Random(seed)
                val stateCount = ln(random.nextInt(STATE_COUNT_LOWER_BOUND, STATE_COUNT_UPPER_BOUND).toDouble()).toInt()
                val maxTransitionCount = ln(
                    random.nextInt(TRANSITION_COUNT_LOWER_BOUND, TRANSITION_COUNT_UPPER_BOUND).toDouble()
                ).toInt()
                val alphabet = ('a'..'z').take(alphabetSize).union(if (hasLambda) setOf(LAMBDA) else setOf()).toList()

                val stateQueue = mutableSetOf(0)
                val transitionsTable = MutableList<MutableMap<Char, MutableSet<Int>>>(stateCount) { mutableMapOf() }
                val notVisitedStates = (1 until stateCount).toMutableSet()

                while (notVisitedStates.isNotEmpty()) {

                    val awaitingStates = mutableSetOf<Int>()
                    val currentState = stateQueue.first()
                    notVisitedStates.remove(currentState)

                    (0 until random.nextInt(maxTransitionCount)).forEach {
                        val letter = alphabet[random.nextInt(alphabet.size)]
                        val targetState = random.nextInt(stateCount)
                        transitionsTable[currentState].getOrPut(letter) { mutableSetOf() }.add(targetState)

                        if (targetState in notVisitedStates)
                            awaitingStates.add(targetState)
                    }

                    stateQueue.addAll(awaitingStates)
                    if (stateQueue.size > 1) {
                        stateQueue.remove(currentState)
                    }
                }

                val terminalStates = (0 until random.nextInt(stateCount))
                    .map { random.nextInt(stateCount) }.toSet()

                return NFA(stateCount, transitionsTable, 0, terminalStates)
            }
        }

        fun removeUnreachableStates(): NFA {
            val unreachableStates = getUnreachableStates().sorted()
            val oldStatesToNewStates = (0 until stateCount).minus(unreachableStates)
                .mapIndexed { newState, oldState -> oldState to newState }
                .toMap()

            val transitionsTableOnlyReachable = transitionsTable
                .filterIndexed { state, _ ->
                    state !in unreachableStates
                }.map { stateTransitions ->
                    stateTransitions.mapValues { (_, targetStates) ->
                        targetStates.map { oldStatesToNewStates.getValue(it) }.toSet()
                    }
                }

            val terminalStatesOnlyReachable = terminalStates
                .filter { it !in unreachableStates }
                .map { oldStatesToNewStates.getValue(it) }.toSet()

            return NFA(
                transitionsTableOnlyReachable.size,
                transitionsTableOnlyReachable,
                oldStatesToNewStates.getValue(startIndex),
                terminalStatesOnlyReachable
            )
        }

        private fun getUnreachableStates(): Set<Int> {
            val queue = transitionsTable[0].values.flatten().toMutableSet()
            val notVisitedStates = transitionsTable.indices.minus(queue).minus(0).toMutableSet()

            while (queue.isNotEmpty()) {
                val currentState = queue.elementAt(0)
                val states = transitionsTable[currentState].values.flatten().toSet()

                notVisitedStates.remove(currentState)
                queue.remove(currentState)
                queue.addAll(notVisitedStates.intersect(states))
            }

            return notVisitedStates
        }

        //region nfa-basic
        override fun check(word: Word): Boolean {
            var index  = 0
            var states: Set<Int> = getLambdaClosure(startIndex).toSet()
            while (states.isNotEmpty() && index < word.length) {
                val ch = word[index]
                states = states
                    .mapNotNull { transitionsTable[it][ch] }
                    .flatten()
                    .distinct()
                    .flatMap { getLambdaClosure(it) }
                    .toSet()
                index++
            }
            return index == word.length && states.any { terminalStates.contains(it) }
        }

        override fun concatenate(other: NFA): NFA {
            val resultStartIndex: Int = this.startIndex
            val resultTerminalStates: Set<Int> = setOf(this.stateCount+other.stateCount)
            val resultStateCount: Int = this.stateCount + other.stateCount + 1
            val mutableTransitionTable = mutableListOf<Map<Char, Set<Int>>>()
            for (i in 0 until this.stateCount) {
                if (i in terminalStates) {
                    mutableTransitionTable.add(this.transitionsTable[i].plus(Pair(LAMBDA, setOf(this.stateCount))))
                } else {
                    mutableTransitionTable.add(this.transitionsTable[i])
                }
            }
            for (i in 0 until other.stateCount) {
                if (i in other.terminalStates) {
                    mutableTransitionTable
                        .add(other.transitionsTable[i]
                            .mapValues{entry -> entry.value.map { it + this.stateCount }.toSet()}
                            .plus(Pair(LAMBDA, resultTerminalStates))
                            )
                } else {
                    mutableTransitionTable.add(other.transitionsTable[i]
                        .mapValues{entry -> entry.value.map { it + this.stateCount }.toSet()})
                }
            }
            mutableTransitionTable.add(emptyMap())
            return NFA(resultStateCount, mutableTransitionTable, resultStartIndex, resultTerminalStates)
        }
        //endregion nfa-basic

        /**
         * Задача:
         * НКА, принимающий дополнение языка текущего НКА
         * и убрать тупики
         * Исполнитель: [А-13-17] Бейдина Кристина
         */
        override fun complement(): NFA {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * Преобразовать к НКА без лямбда-переходов
         * Исполнитель: [А-05-17] Кощин Евгений
         */
        fun removeLambdaTransitions(): NFA {
            // copy all transitions except lambda
            val lambdaFreeTransitionTable =
                this.transitionsTable.map { transitions ->
                    transitions.filter { it.key != LAMBDA }
                        .map { it.key to it.value.toMutableSet() }
                        .toMap()
                        .toMutableMap()
                }.toMutableList()

            val lambdaFreeTerminalStates = this.terminalStates.toMutableSet()

            // remove lambda by adding transitions
            this.transitionsTable.indices.forEach { state ->
                if (state > lambdaFreeTransitionTable.lastIndex)
                    lambdaFreeTransitionTable.add(mutableMapOf())

                val lambdaClosure = getLambdaClosure(state)
                if (isTerminal(lambdaClosure))
                    lambdaFreeTerminalStates.add(state)

                getNonLambdaClosure(lambdaClosure).forEach {
                    lambdaFreeTransitionTable[state].getOrPut(it.key) { mutableSetOf() }.addAll(it.value)
                }
            }

            return NFA(this.stateCount, lambdaFreeTransitionTable, this.startIndex, lambdaFreeTerminalStates)
        }

        /**
         * Returns a set of states that can be achieved only through
         * lambda from state 's'
         */
        private fun getLambdaClosure(s: Int): Set<Int> {
            val visitedStates = mutableSetOf(s)

            val stateQueue = this.transitionsTable[s][LAMBDA].orEmpty().toMutableSet()
            while (stateQueue.isNotEmpty()) {

                val statesToAdd = stateQueue.flatMap { state ->
                    this.transitionsTable[state][LAMBDA].orEmpty()
                }.toSet()

                visitedStates.addAll(stateQueue)
                stateQueue.clear()
                stateQueue.addAll(statesToAdd.minus(visitedStates))
            }

            return visitedStates
        }

        /**
         * Returns a map of states that can be achieved from lambdaClosure
         * through the only one letter transition
         */
        private fun getNonLambdaClosure(lambdaClosure: Set<Int>): Map<Letter, Set<Int>> {
            return lambdaClosure.flatMap {
                this.transitionsTable[it].entries
            }.filter { it.key != LAMBDA }
                .map { it.key to it.value }.toMap()
        }

        /**
         * Returns true if state with provided lambdaClosure have to become terminal
         */
        private fun isTerminal(lambdaClosure: Set<Int>): Boolean {
            return lambdaClosure.any { it in this.terminalStates }
        }


        /**
         * Задача:
         * пересечение и объединение НКА
         */
        //region nfa-intersection
        override fun intersect(other: NFA): NFA {
            TODO("Not yet implemented")
        }

        override fun union(other: NFA): NFA {
            TODO("Not yet implemented")
        }
        //endregion

        /**
         * Задача:
         * НКА, принимающий реверсию всех слов языка текущего НКА
         * Ограничение: единственное начальное состояние [Морозова Дарья А-13-17]
         */
        override fun reversed(): NFA {
            val reversedTransitions =
                List<MutableMap<Char, MutableSet<Int>>>(stateCount + 1) { mutableMapOf() }

            transitionsTable.forEachIndexed { state1, transition ->
                transition.forEach { (letter, stateSet) ->
                    stateSet.forEach { state2 ->
                        reversedTransitions[state2].getOrPut(letter) { mutableSetOf() }.add(state1)
                    }
                }
            }

            reversedTransitions[reversedTransitions.lastIndex][LAMBDA] = terminalStates.toMutableSet()
            return NFA(
                reversedTransitions.size,
                reversedTransitions,
                reversedTransitions.lastIndex,
                setOf(startIndex)
            )
        }

        /**
         * Задача:
         * звезда Клини
         * Исполнитель: [А-05-17] Бабенов Артем
         */

        //region nfa-closure

        /**
         * Для построения НКА для итерации (операции Клини) необходимо определить новое начальное состояние,
         * которое будет ещё и конечным (для определения пустого слова как часть языка),
         * и из всех конечных состояний организовать ε(λ)-переход в прежнее начальное состояние автомата
         *
         * P.S. Определять новое начальное состояние необходимо только в том случае,
         * когда пустое слово не определено как часть языка (если начальное состояние не является конечным)
         */
        override fun starClosure(): NFA {
            val newStartIndex: Int
            val newTerminalStates: Set<Int>
            val newTransitionsTable: List<MutableMap<Char, Set<Int>>>

            if (this.terminalStates.contains(this.startIndex)) {
                newStartIndex = this.startIndex
                newTerminalStates = this.terminalStates
                newTransitionsTable = this.transitionsTable.map { it.toMutableMap() }
            } else {
                // добавление нового терминального состояния (которое в дальнейшем будет стартовым для полученного НКА)
                newStartIndex = this.stateCount
                newTerminalStates = this.terminalStates.plus(this.stateCount)
                newTransitionsTable = this.transitionsTable.map { it.toMutableMap() }.plus(mutableMapOf())
            }

            newTerminalStates.forEach {
                // добавление лямбда-перехода из каждого терминального состояния в текущее стартовое состояние
                newTransitionsTable[it][LAMBDA] = newTransitionsTable[it][LAMBDA].orEmpty().plus(this.startIndex)
            }

            return NFA(
                newTransitionsTable.size,   // добавлено одно новое состояние (или не добавлено)
                newTransitionsTable,
                newStartIndex,   // новое начальное состояние (или не новое)
                newTerminalStates
            )
        }

        /**
         * Для построения НКА для плюсового замыкания (звезда Клини без пустого слова)
         * необходимо из всех конечных состояний организовать ε(λ)-переход в начальное состояние автомата
         */
        override fun plusClosure(): NFA {
            val newTransitionsTable = this.transitionsTable.map { it.toMutableMap() }

            this.terminalStates.forEach {
                // добавление лямбда-перехода из каждого терминального состояния в стартовое состояние
                newTransitionsTable[it][LAMBDA] = newTransitionsTable[it][LAMBDA].orEmpty().plus(this.startIndex)
            }

            return NFA(
                this.stateCount,
                newTransitionsTable,
                this.startIndex,
                this.terminalStates
            )
        }

        //endregion nfa-closure


        enum class NFAToRegExpConversionAlgorithm {
            Kleene,
            StateElimination
        }

        fun toRegExp(
            algorithm: NFAToRegExpConversionAlgorithm = NFAToRegExpConversionAlgorithm.StateElimination
        ): RegularExpression {
            return when (algorithm) {
                NFAToRegExpConversionAlgorithm.Kleene -> kleeneConversion()
                NFAToRegExpConversionAlgorithm.StateElimination -> stateElimination()
            }
        }

        /**
         * Задача:
         * Метод удаления состояний
         */
        private fun stateElimination(): RegularExpression {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * алгоритм Клини
         *   Исполнитель: [A-13-17] Аксенов Даня
         * @see https://en.wikipedia.org/wiki/Kleene%27s_algorithm
         */
        private fun kleeneConversion(): RegularExpression {
            val initTableWithRegulars: List<MutableList<RegularExpression?>> = List(stateCount) {
                mutableListOf<RegularExpression?>()
            }

            for (i in 0 until stateCount) {
                for (j in 0 until stateCount) {
                    initTableWithRegulars[i].add(label(i, j))
                }
            }
            for (i in 0 until stateCount) {
                if (initTableWithRegulars[i][i] == null)
                    initTableWithRegulars[i][i] = RegularExpression(RegularExpression.Component.Lambda)
                else {
                    initTableWithRegulars[i][i] = initTableWithRegulars[i][i]!!.union(
                        RegularExpression(RegularExpression.Component.Lambda)
                    )
                }
            }

            var newTableWithRegulars: List<List<RegularExpression?>> = initTableWithRegulars

            for (k in 0 until stateCount) {
                newTableWithRegulars = getNewTableWithRegulars(newTableWithRegulars, k)

            }

            return terminalStates.mapNotNull { newTableWithRegulars[startIndex][it] }
                .reduce { unionsOfRegExes, currentRegEx ->
                    unionsOfRegExes.union(currentRegEx)
                }
        }

        @Suppress("NestedBlockDepth")
        private fun getNewTableWithRegulars(
            previousTableWithRegulars: List<List<RegularExpression?>>,
            k: Int
        ): List<List<RegularExpression?>> {
            val newTableWithRegulars: List<MutableList<RegularExpression?>> = List(stateCount) {
                mutableListOf<RegularExpression?>()
            }
            for (i in 0 until stateCount) {
                for (j in 0 until stateCount) {
                    val cIK = previousTableWithRegulars[i][k]
                    val cKK = previousTableWithRegulars[k][k]
                    val cKJ = previousTableWithRegulars[k][j]
                    val cIJ = previousTableWithRegulars[i][j]
                    if ((cIK == null) || (cKK == null) || (cKJ == null)) {
                        newTableWithRegulars[i].add(cIJ)
                    } else {
                        newTableWithRegulars[i].add(cIK.concatenate(cKK.starClosure()).concatenate(cKJ))
                        if (cIJ != null)
                            newTableWithRegulars[i][j] = newTableWithRegulars[i][j]!!.union(cIJ)
                    }
                }
            }
            return newTableWithRegulars
        }

        private fun label(i: Int, j: Int): RegularExpression? {
            val letters = transitionsTable[i].filter { j in it.value }.keys.map {
                RegularExpression.Component.Letter(it)
            }
            return when (letters.size) {
                0 -> null
                1 -> RegularExpression(letters.first())
                else -> RegularExpression(
                    RegularExpression.Component.NAryOperator.Alternation(letters)
                )
            }
        }


        /**
         * Задача:
         * алгоритм построения подмножеств
         * Исполнитель: [A-13-17] Шаронова Елена
         */

        fun toDFA(): DFA {
            val alphabet = getAlphabet()
            val subsetTransitionTable = getSubsetTransitionTable(alphabet)
            val nodesToIntAssociation = subsetTransitionTable.keys.toList()
            val newTransitionsTable = subsetTransitionTable.mapValues {
                it.value.mapValues { x -> nodesToIntAssociation.indexOf(x.value) }
            }.values.toList()
            val newTerminals = subsetTransitionTable.keys.filter { it.any { x -> x in terminalStates } }.map {
                nodesToIntAssociation.indexOf(it)
            }.toSet()
            val newStartState = nodesToIntAssociation.indexOf(getLambdaClosure(startIndex))

            return DFA(
                newTransitionsTable.size,
                alphabet,
                newTransitionsTable,
                newStartState,
                newTerminals
            )
        }

        private fun getAlphabet(): CharAlphabet {
            return transitionsTable.flatMap { it.keys }.filterNot { it == LAMBDA }.toSet()
        }

        /**
         * Алгоритм:
         * 1) берем вершину (сначала стартовую?) -> переводим в узел (Set<Int>)
         * 2) для заданного узла для каждой буквы алфавита составляем новый узел
         *      из всех тех вершин, в которые переходят все вершины из узла заданного
         *      и составляем соответствующие правила
         * 3) для всех ранее несуществующих узлов вернуться к 2
         * 4)начальное состояние остается, терминальные - все те узлы, в составе которых есть терм состояния
         *
         */

        /**
        stateSubset - везде набор состояние ({q0,q1})
         */

        private fun makeNewRuleForStateSubset(node: Set<Int>, alphabet: CharAlphabet): Map<Char, Set<Int>> {
            return alphabet.associateWith { letter ->
                node.flatMap {
                    transitionsTable[it].getOrElse(letter, { setOf() })
                }.flatMap { getLambdaClosure(it) }.toSet()
            }
        }

        private fun getSubsetTransitionTable(alphabet: CharAlphabet): Map<Set<Int>, Map<Char, Set<Int>>> {
            val subsetTransitionTable: MutableMap<Set<Int>, Map<Char, Set<Int>>> = mutableMapOf()
            val initialSubset = getLambdaClosure(startIndex)
            val stateSubsetQueue: MutableList<Set<Int>> = mutableListOf(initialSubset)
            while (stateSubsetQueue.isNotEmpty()) {
                val stateSubset = stateSubsetQueue.removeAt(0).toMutableSet()

                val ruleForCurrentStateSubset = makeNewRuleForStateSubset(stateSubset, alphabet)
                subsetTransitionTable[stateSubset] = ruleForCurrentStateSubset
                val newStateSubsets =
                    ruleForCurrentStateSubset.values.minus(subsetTransitionTable.keys).toSet()
                stateSubsetQueue.addAll(newStateSubsets)
            }
            return subsetTransitionTable
        }


        /**
         * Задача:
         * Приведение к правой регулярной грамматике
         *	Исполнитель: [А-13-17] Маресина Александра
         */
        @ExperimentalUnsignedTypes
        fun toRightGrammar(): RegularGrammar.RightRegularGrammar {
            val rules = mutableMapOf<NonTerminalSymbol, MutableSet<RegularGrammarRule>>()
            val thisLambdaFree = this.removeLambdaTransitions()

            thisLambdaFree.transitionsTable.forEachIndexed { state, transitions ->
                for ((letter, targetStates) in transitions) {
                    for (targetState in targetStates) {
                        val nonTerminal = NonTerminalSymbol('S', targetState.toUInt())

                        rules.getOrPut(NonTerminalSymbol('S', state.toUInt())) { mutableSetOf() }.add(
                            RegularGrammarRule.NonTerminalRule.RightRule(letter, nonTerminal)
                        )
                    }
                }
            }

            thisLambdaFree.terminalStates.forEach { state ->
                rules.getOrPut(NonTerminalSymbol('S', state.toUInt())) { mutableSetOf() }.add(
                    RegularGrammarRule.EmptyRule
                )
            }

            return RegularGrammar.RightRegularGrammar(NonTerminalSymbol('S'), rules)
        }

        /**
         * Задача:
         * Приведение к левой регулярной грамматике
         *	Исполнитель: [А-13-17] Рощупкин Александр
         */
        @ExperimentalUnsignedTypes
        fun toLeftGrammar(): RegularGrammar.LeftRegularGrammar {
            val rules = mutableMapOf<NonTerminalSymbol, MutableSet<RegularGrammarRule>>()
            val thisLambdaFree = this.removeLambdaTransitions()

            rules[NonTerminalSymbol('S', 1u)] =
                mutableSetOf<RegularGrammarRule>(RegularGrammarRule.EmptyRule)
            thisLambdaFree.transitionsTable.forEachIndexed { state, transitions ->
                for ((letter, targetStates) in transitions) {
                    for (targetState in targetStates) {
                        val nonTerminal = NonTerminalSymbol('S', targetState.inc().toUInt())

                        rules.getOrPut(nonTerminal) { mutableSetOf() }.add(
                            RegularGrammarRule.NonTerminalRule.LeftRule(
                                letter,
                                NonTerminalSymbol('S', state.inc().toUInt())
                            )
                        )
                    }
                }
            }

            rules[NonTerminalSymbol('S')] = thisLambdaFree.terminalStates
                .map { NonTerminalSymbol('S', it.inc().toUInt()) }
                .flatMap { rules.getValue(it) }
                .toMutableSet()

            return RegularGrammar.LeftRegularGrammar(NonTerminalSymbol('S'), rules)
        }
    }
}

/**
 * Задача
 * Алгоритм Ахо-Корасика
 *  @see https://en.wikipedia.org/wiki/Aho%E2%80%93Corasick_algorithm
 *	Исполнитель: [А-13-17] Полянский Родион
 */
//region aho-corasick

/**
 * Алгоритм Ахо-Корасик реализует поиск мн-ва паттернов из списка в данной строке.
 * Результат - отображение: индекс из данной строки -> список паттернов из списка, начинающаяся с этого индекса.
 */
fun ahoCorasick(patterns: List<String>, text: String): Map<Int, List<String>> {
    return AhoCorasick(Bor.of(patterns)).processText(text)
}

data class Bor(
    val patterns: List<String>,                      // переданные паттерны строк
    val nodes: List<BorNode>                         // список узлов бора
) {
    data class BorNode(
        val charToParent: Char,                      // символ, ведущий к родителю
        val parent: Int? = null,                     // индекс родителя
        val nextNodes: Map<Char, Int> = mapOf(),     // выходящие узлы
        val patternNum: Int? = null,                 // номер паттерна, за который отвечает терминал
        val isLeaf: Boolean = false                  // флаг, является ли вершина терминалом
    )

    companion object {
        fun of(patterns: List<String>): Bor {
            var nodes = listOf(BorNode('$'))

            for (i in 0 until patterns.size) {
                nodes = addPattern(nodes, patterns[i], i)
            }

            return Bor(patterns, nodes)
        }

        const val ROOT = 0

        private fun addPattern(nodes: List<BorNode>, str: String, patternNum: Int): List<BorNode> {
            var node = ROOT
            val newNodes = nodes.toMutableList()

            for (ch in str) {
                if (!newNodes[node].nextNodes.containsKey(ch)) {
                    newNodes.add(BorNode(charToParent = ch, parent = node))

                    val newNextNodes = newNodes[node].nextNodes.toMutableMap()
                    newNextNodes[ch] = newNodes.size - 1

                    newNodes[node] = newNodes[node].copy(nextNodes = newNextNodes)
                }

                node = newNodes[node].nextNodes[ch]!!
            }

            newNodes[node] = newNodes[node].copy(isLeaf = true, patternNum = patternNum)

            return newNodes
        }
    }
}

/**
 * Алгоритм Ахо-Корасик состоит из 3 шагов:
 * 1) Построение бора из переданных паттернов.
 * 2) Построение суффиксных ссылок.
 * 3) Построение сжатых суффиксных ссылок.
 */
class AhoCorasick(private val bor: Bor) {
    // массив из переходов, используемых для вычисления суффиксных ссылок
    private var suffNodes: Array<MutableMap<Char, Int>> = Array(bor.nodes.size) { mutableMapOf<Char, Int>() }
    private var suffLinks: Array<Int?> = arrayOfNulls(bor.nodes.size)           // массив суффиксных ссылок
    private var comprSuffLinks: Array<Int?> = arrayOfNulls(bor.nodes.size)      // массив сжатых суффиксных ссылок

    /**
     * Получает суффиксную ссылку, т.е. для вершины v это указатель на вершину u, в котором
     * строка в u - наибольший cобственный суффикс строки v.
     */
    private fun getSuffLink(ind: Int?): Int? {
        val node = bor.nodes[ind!!]

        if (suffLinks[ind] == null) {
            if (ind == Bor.ROOT || node.parent == Bor.ROOT) {
                suffLinks[ind] = Bor.ROOT
            } else {
                suffLinks[ind] = getLink(getSuffLink(node.parent), node.charToParent)
            }
        }

        return suffLinks[ind]
    }

    /**
     * Реализует функцию перехода по символу из текущего состояния:
     * 1) если есть возможность перейти по символу, то делаем это;
     * 2) иначе, переходим по суффиксной ссылке и там делаем переход по символу.
     *
     * (суффиксная ссылка корня указывает на корень.)
     */
    private fun getLink(ind: Int?, ch: Char): Int {
        val node = bor.nodes[ind!!]

        if (!suffNodes[ind].containsKey(ch)) {
            suffNodes[ind][ch] = when {
                node.nextNodes[ch] != null && node.nextNodes[ch]!! > 0 -> node.nextNodes[ch]!!
                ind == Bor.ROOT -> Bor.ROOT
                else -> getLink(getSuffLink(ind), ch)
            }
        }

        return suffNodes[ind][ch]!!
    }

    /**
     * Вычисляет сжатую суффиксную ссылку, т.е. ближайший терминал перехода по суффиксным ссылкам.
     *
     * (сжатая суффиксная ссылка корня указывает на корень.)
     */
    private fun getComprSuffLink(ind: Int?): Int? {
        if (comprSuffLinks[ind!!] == null) {

            comprSuffLinks[ind] = when {
                bor.nodes[getSuffLink(ind)!!].isLeaf -> getSuffLink(ind)
                getSuffLink(ind) == Bor.ROOT -> Bor.ROOT
                else -> getComprSuffLink(getSuffLink(ind))
            }
        }

        return comprSuffLinks[ind]
    }

    /**
     * Ищет совпадения с паттернами, проходясь по сжатым суффиксным ссылкам.
     */
    private fun checkMatching(
        node: Int?,
        indInText: Int,
        match: MutableMap<Int, MutableList<String>>
    ) {
        var u = node

        while (u != Bor.ROOT) {
            if (bor.nodes[u!!].isLeaf) {
                val pattern = bor.patterns[bor.nodes[u].patternNum!!]
                val ind = indInText - pattern.length + 1

                if (match[ind] == null) {
                    match[ind] = mutableListOf()
                }

                match[ind]?.add(pattern)
            }

            u = getComprSuffLink(u)
        }
    }

    /**
     * Реализует поиск паттернов в переданной строке.
     */
    fun processText(text: String): Map<Int, List<String>> {
        var node = Bor.ROOT
        val match = mutableMapOf<Int, MutableList<String>>()

        for (i in 0 until text.length) {
            node = getLink(node, text[i])
            checkMatching(node, i + 1, match)
        }

        return match
    }

}

//endregion aho-corasick

/**
 * Задача
 * Алгоритм Бертранда, альтернатива Ахо-Карасику
 * @see http://se.ethz.ch/~meyer/publications/string/string_matching.pdf
 * Исполнитель: [А-05-17] Крылов Кирилл
 */
fun bertrand(/*TODO*/) {
    TODO("Not yet implemented")
}
