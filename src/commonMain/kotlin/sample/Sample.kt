package sample

expect class Sample {
    fun checkMe(): Int
}

expect object Platform {
    val name: String
}

class TestTarget {
    fun test(): TestTarget {
        return toAnotherEntity().toTestTarget()
    }

    fun toAnotherEntity(): AnotherEntity {
        return AnotherEntity(1)
    }

    fun unimplementedFunctionCaller() = unimplementedFunction()

    fun unimplementedFunction(): AnotherEntity {
        TODO()
    }
}

class AnotherEntity(val index: Int) {
    fun toTestTarget(): TestTarget {
        TODO()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class != other::class) return false

        other as AnotherEntity

        if (index != other.index) return false

        return true
    }

    override fun hashCode(): Int {
        return index
    }

}


class CommonTestable {
    fun helloCommon() = "Now you can test me in common module"
}

fun hello(): String = "Hello from ${Platform.name}"

